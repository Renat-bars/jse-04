# THE REAL WORLD

## DEVELOPER INFO

name: Renat Almukhametov

e-mail: rralmukhametov@tsconsulting.com

e-mail: renat-ralmukhametov@yandex.ru

## HARDWARE

CPU: i7

RAM: 16Gb

SSD: 256Gb

## SOFTWARE

System: Windows 10 Pro

Version JDK: 1.8.0_281

## PROGRAM RUN

```
java -jar ./task-manager.jar

```
## SCREENSHOTS

https://disk.yandex.ru/d/GjFIqWBBBAEd6g?w=1


